 PREFIX : <http://ekaw#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 

SELECT ?person 	WHERE {
?person <http://confOf#writes> ?paper. 
?person rdf:type <http://confOf#Participant>. 
?person <http://confOf#earlyRegistration> ?variable_temp0. FILTER(?variable_temp0 = "false"^^<http://www.w3.org/2001/XMLSchema#boolean>).


?paper a <http://confOf#Poster>. 
}
