PREFIX : <http://ekaw#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 


SELECT ?person 	WHERE {
?person <http://confOf#writes> ?paper. 
?paper a <http://confOf#Paper>. 
?person rdf:type <http://confOf#Participant>. 
?person <http://confOf#earlyRegistration> ?variable_temp0. FILTER(?variable_temp0 = "true"^^<http://www.w3.org/2001/XMLSchema#boolean>).


}