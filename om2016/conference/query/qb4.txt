PREFIX : <http://ekaw#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?author WHERE {
?paper :writtenBy ?author.
?paper rdf:type :Regular_Paper.
}