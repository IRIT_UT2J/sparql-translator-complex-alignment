PREFIX : <http://ekaw#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
SELECT ?person WHERE {
?person :authorOf ?paper.
?paper a :Paper.
{?person rdf:type :PC_Chair.}
UNION
{?person rdf:type :Session_Chair.}
UNION
{?person rdf:type :Demo_Chair.}
UNION
{?person rdf:type :OC_Chair.}
UNION
{?person rdf:type :Tutorial_Chair.}
}